package com.twuc.web;

import com.twuc.ApiTestBase;
import com.twuc.domain.Order;
import com.twuc.repository.OrderRepository;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


class OrderControllerTest extends ApiTestBase {

    @Autowired
    private OrderRepository orderRepo;

    @Disabled
    @Test
    void should_get_all_orders() throws Exception {
        List<Order> orders = orderRepo.findAll();
        mockGet("/api/orders")
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(serialize(orders)));
    }
}