package com.twuc.web;

import com.twuc.ApiTestBase;
import com.twuc.contract.ProductRequest;
import com.twuc.domain.Product;
import com.twuc.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.math.BigDecimal;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ProductControllerTest extends ApiTestBase {

    @Autowired
    private ProductRepository productRepo;

    @Test
    void should_get_products() throws Exception {
        List<Product> products = productRepo.findAll();
        String serializeProducts = serialize(products);
        mockGet("/api/products")
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(serializeProducts));
    }

    @Test
    void should_get_product_by_id_when_no_products() throws Exception {
        mockGet("/api/products/1")
                .andExpect(status().isNotFound());
    }

    @Test
    void should_create_product() throws Exception {
        ProductRequest productRequest =
                new ProductRequest("coco cola", new BigDecimal(1.00),
                        "瓶", "aaa");
        mockPost("/api/products", productRequest, MediaType.APPLICATION_JSON_UTF8)
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }
}
