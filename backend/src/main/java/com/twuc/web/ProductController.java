package com.twuc.web;

import com.twuc.contract.ProductRequest;
import com.twuc.domain.Product;
import com.twuc.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin(origins = "*")
public class ProductController {

    @Autowired
    private ProductRepository productRepo;

    @GetMapping("/api/products")
    public ResponseEntity getProducts(){
        List<Product> products = productRepo.findAll();
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(products);
    }

    @GetMapping("/api/products/{id}")
    public ResponseEntity getProductById(@PathVariable Long id){
        Product product = productRepo.findById(id).get();
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(product);
    }

    @ExceptionHandler
    public ResponseEntity handleException(NoSuchElementException exception){
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @PostMapping("/api/products")
    public ResponseEntity createProduct(@RequestBody ProductRequest productRequest){
        Product product = productRequest.toProduct();
        productRepo.save(product);

        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header("Access-Control-Expose-Headers", "Location")
                .header("Location", "/api/products/"+product.getId())
                .build();
    }
}
