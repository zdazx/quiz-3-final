package com.twuc.domain;

import com.twuc.contract.ProductRequest;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 64)
    private String name;

    @Column(nullable = false, length = 32)
    private BigDecimal price;

    @Column(nullable = false, length = 2)
    private String unit;

    @Column(nullable = false, length = 128)
    private String imgUrl;

    public Product() {
    }

    public Product(String name, BigDecimal price, String unit, String imgUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imgUrl = imgUrl;
    }

    public Product(ProductRequest productRequest) {
        this.name = productRequest.getName();
        this.price = productRequest.getPrice();
        this.unit = productRequest.getUnit();
        this.imgUrl = productRequest.getImgUrl();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImgUrl() {
        return imgUrl;
    }
}
