package com.twuc.domain;

import com.twuc.contract.OrderRequest;

import javax.persistence.*;

@Entity
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Long quantity;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    public Order() {
    }

    public Order(Long quantity) {
        this.quantity = quantity;
    }

    public Order(OrderRequest orderRequest) {
        this.quantity = orderRequest.getQuantity();
        this.product = orderRequest.getProductRequest().toProduct();
    }

    public Long getId() {
        return id;
    }

    public Long getQuantity() {
        return quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
