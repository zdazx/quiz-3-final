package com.twuc.contract;

import com.twuc.domain.Order;

import javax.validation.constraints.NotNull;

public class OrderRequest {

    @NotNull
    private Long quantity;

    private ProductRequest productRequest;

    public OrderRequest() {
    }

    public OrderRequest(@NotNull Long quantity, ProductRequest productRequest) {
        this.quantity = quantity;
        this.productRequest = productRequest;
    }

    public Long getQuantity() {
        return quantity;
    }

    public ProductRequest getProductRequest() {
        return productRequest;
    }

    public Order toOrder(){
        return new Order(this);
    }
}
