package com.twuc.contract;

import com.twuc.domain.Product;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class ProductRequest {
    @NotNull
    @Size(min = 1, max = 64)
    private String name;

    @NotNull
    private BigDecimal price;

    @NotNull
    @Size(min = 1, max = 2)
    private String unit;

    @NotNull
    @Size(min = 1, max = 128)
    private String imgUrl;

    public ProductRequest() {
    }

    public ProductRequest(@NotNull @Size(min = 1, max = 64) String name,
                          @NotNull BigDecimal price,
                          @NotNull @Size(min = 1, max = 2) String unit,
                          @NotNull @Size(min = 1, max = 128) String imgUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imgUrl = imgUrl;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public Product toProduct(){
        return new Product(this);
    }
}
