create table order
(
    id         bigint auto_increment,
    quantity   bigint not null,
    product_id bigint,
    primary key (id),
    foreign key (product_id) references product(id)
);