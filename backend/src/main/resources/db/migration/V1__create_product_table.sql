create table product (
       id bigint AUTO_INCREMENT ,
        img_url varchar(128) not null,
        name varchar(64) not null,
        price decimal(19,2) not null,
        unit varchar(2) not null,
        primary key (id)
    )