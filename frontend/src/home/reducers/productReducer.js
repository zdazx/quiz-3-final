const initState = {
  products: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_PRODUCTS' :
      return {
        ...state,
        products: action.products
      };
    default:
      return state;
  }
};