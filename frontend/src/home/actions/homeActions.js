export const getProducts = () =>(dispatch) => {
  fetch('http://localhost:8080/api/products')
    .then(response => response.json())
    .then(response => {
      dispatch({
        type: 'GET_PRODUCTS',
        products: response
      });
    });
};