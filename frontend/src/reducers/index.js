import {combineReducers} from "redux";
import productReducer from "../home/reducers/productReducer";
import productCreateReducer from "../ProductCreate/reducers/productCreateReducer";

const reducers = combineReducers({
  product: productReducer,
  productCreate: productCreateReducer
});
export default reducers;