import React, {Component} from 'react';
import './App.less';
import {Route, BrowserRouter as Router, Switch} from "react-router-dom";
import Home from "./home/pages/Home";
import ProductCreate from "./ProductCreate/pages/ProductCreate";

class App extends Component{
  render() {
    return (
      <div className='App'>
        <Router>
          <Switch>
            <Route path={'/products/create'} component={ProductCreate} />
            <Route path="/" component={Home} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;