import React, {Component} from 'react';
import {MdHome, MdShoppingCart, MdAdd} from "react-icons/md";
import {Link} from "react-router-dom";
import '../less/Header.less'

class Header extends Component {
  render() {
    return (
      <div className={'header'}>

        <div className={'title'}>
          <Link to={'/'}>
            <MdHome />
            <span className={'titleText'}>商城</span>
          </Link>
        </div>

        <div className={'title'}>
          <Link to={'/'}>
            <MdShoppingCart />
            <span className={'titleText'}>订单</span>
          </Link>
        </div>

        <div className={'title'}>
          <Link to={'/products/create'}>
            <MdAdd />
            <span className={'titleText'}>添加商品</span>
          </Link>
        </div>

      </div>
    );
  }
}

export default Header;