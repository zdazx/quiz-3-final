import React, {Component} from 'react';
import Header from "../../header/pages/Header";
import {bindActionCreators} from "redux";
import {Link} from "react-router-dom";
import {createProduct} from "../actions/productActions";
import {connect} from "react-redux";
import '../less/productCreate.less'

class ProductCreate extends Component {

  constructor (props){
    super(props);
    this.backHome = this.backHome.bind(this);
}

  createProduct(){
    const product = {
      name: document.getElementById('name').value,
      price: document.getElementById('price').value,
      unit: document.getElementById('unit').value,
      imgUrl: document.getElementById('imgUrl').value
    };

    this.props.createProduct(product, this.backHome);
  }

  backHome(){
    this.props.history.push('/');
  }

  render() {
    return (
      <div>
        <Header />
        <div className={'createSection'}>
          <h1>添加商品</h1>

          <label htmlFor={'name'}><span className={'start'}>*</span>名称：</label>
          <input type={'text'} id={'name'} placeholder={'名称'} required/>

          <label htmlFor={'price'}><span className={'start'}>*</span>价格：</label>
          <input type={'text'} id={'price'} placeholder={'价格'} required/>

          <label htmlFor={'unit'}><span className={'start'}>*</span>单位：</label>
          <input type={'text'} id={'unit'} placeholder={'单位'} required/>

          <label htmlFor={'imgUrl'}><span className={'start'}>*</span>图片：</label>
          <input type={'text'} id={'imgUrl'} placeholder={'URL'} required/>

          <button className={'addProduct'} onClick={this.createProduct.bind(this)}><Link to={'/'}>提交</Link></button>

        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {

  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  createProduct
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps) (ProductCreate);