export const createProduct = (product, callback) => (dispatch) => {
  fetch('http://localhost:8080/api/products', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(product)
  }).then(response => {
    callback();
    dispatch({
      type: 'CREATE_PRODUCT'
    });
  })
};