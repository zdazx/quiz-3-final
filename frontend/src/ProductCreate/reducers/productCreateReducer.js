const initState = {};

export default (state = initState, action) => {
  switch (action.type) {
    case 'CREATE_PRODUCT':
      return {
        ...state
      };
    default:
      return state;
  }
};